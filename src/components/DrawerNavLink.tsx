import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import React, { FC } from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';
import { useTheme } from '@mui/material';

interface ListItemLinkProps {
  icon: React.ReactElement;
  label: string;
  to: string;
}

const ListItemLink: FC<ListItemLinkProps> = ({
  icon,
  label,
  to,
}): JSX.Element => {
  const theme = useTheme();

  const activeStyle = {
    color: theme.palette.primary.main,
  };

  const renderLink = React.useMemo(
    () =>
      React.forwardRef<HTMLAnchorElement, Omit<NavLinkProps, 'to'>>(
        (itemProps, ref) => (
          <NavLink
            to={to}
            ref={ref}
            {...itemProps}
            style={({ isActive }) => (isActive ? activeStyle : {})}
          />
        )
      ),
    [to]
  );
  return (
    <li>
      <ListItem button component={renderLink}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
      <Divider />
    </li>
  );
};

export default ListItemLink;
