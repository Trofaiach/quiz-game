import { createTheme, ThemeProvider } from '@mui/material';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import BackOffice from './pages/BackOffice';
import Game from './pages/Game';
import Rankings from './pages/Rankings';
import HomePage from './pages/HomePage';
import ErrorPage from './pages/404';
import Layout from './components/Layout';

const theme = createTheme({
  palette: {
    primary: {
      main: '#009688',
    },
    secondary: {
      main: '#f4f4f8',
    },
  },
});

const App = () => {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <Layout>
          <Routes>
            <Route path="/" element={<HomePage />}></Route>
            <Route path="/game" element={<Game />}></Route>
            <Route path="/rankings" element={<Rankings />}></Route>
            <Route path="/back" element={<BackOffice />}></Route>
            <Route path="*" element={<ErrorPage />}></Route>
          </Routes>
        </Layout>
      </ThemeProvider>
    </Router>
  );
};

export default App;
