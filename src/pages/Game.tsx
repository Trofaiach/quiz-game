import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import React, { useState } from 'react';

const Game = () => {
  const [username, setUsername] = useState('');
  const [isValid, setIsValid] = useState(true);
  const storedUsername = localStorage.getItem('username');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value);
    setIsValid(true);
  };
  const validation = () => {
    !!username ? localStorage.setItem('username', username) : setIsValid(false);
  };

  return (
    <Paper
      sx={{
        display: 'flex',
        flexDirection: 'column',
        width: { md: '50%' },
        height: { md: '90%' },
        mt: '20px',
        p: '20px',
        gap: '10px',
        alignItems: 'center',
      }}
      elevation={4}
    >
      <Typography
        variant="h6"
        sx={{ mb: '10px', textAlign: 'center', whiteSpace: 'pre-line' }}
      >
        {!storedUsername
          ? 'Enter new username'
          : `Continue as ${storedUsername} \n or \n Enter new username`}
      </Typography>
      <TextField
        error={!isValid}
        helperText={!isValid && 'Username is required'}
        id="outlined-basic"
        label="Username"
        variant="outlined"
        autoComplete="off"
        size="small"
        sx={{ mb: '20px', input: { textAlign: 'center' } }}
        value={username}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange(e)}
      />
      <Button variant="contained" onClick={validation}>
        Continue
      </Button>
    </Paper>
  );
};
export default Game;
